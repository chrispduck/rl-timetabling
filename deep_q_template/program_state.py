import torch
import torch.nn as nn

class ProgramState:
    def __init__(self, n):
        """ Sets and holds the program state
        Comprised of 3 objects
        An office vector, o, indicating which people are currently in the office with 1 and at-home with 0.
        An nxn distance matrix, d, recording the number of days in which person i has seen person j
        A constraint vector, c, which monitors the number of consecutive days that people have been out of the ofice
        
        Args:
            n (int): total number of employeees
        """
        super().__init__()
        self.o = torch.zeros([1, n]) # office
        self.d = torch.zeros([n, n]) # distance
        self.c = torch.zeros([1, n]) # constraint
