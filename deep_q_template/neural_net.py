import torch
import torch.nn as nn
import torch.nn.functional as F
from program_state import ProgramState
import numpy as np
from typing import Union

class QNet(nn.module):
    def __init__(self, n):
        """ Neural network Q function specific to our problem
        Args:
            n (int): number of people
        """
        super(Q, self).__init__()
        # Input size of n x n 
        # 1 input channel, 6 output channels, kxk kernel size
        k = 3
        self.conv1 = nn.Conv2d(1, 6, k)
        self.conv2 = nn.Conv2d(6, 16, k)
        l_conv = 16*6*6         #1 6x6x6 elements from conv layer
        
        # Fully connected layers (input size, output size)
        self.fc1 = nn.Linear(l_conv + 2*n , 120) # add 2n from office and constraint vectors, 120 output neurons
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 1) 
        
    def forward(self, state:ProgramState):
        # Apply convolutions to D matrix
        # Max pooling over 2x2 window
        x = F.max_pool2d(F.relu(self.conv1(state.d)), (2,2))
        # If the size is a square you can only specify a single number
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1)
        # Concatenate GNN output with office and constraint vectors 
        x = torch.cat([x, state.o, state.c], dim=0)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x
    
    def train(self, input, target):
        """ Train the above defined Net to predict Q better. 

        Args:
            input (torch.Tensor): The input state to the network.
            target (float): The target value of Q

        Returns:
            (int): zero if training iteration successful
        """
        #TODO
        
        
        return 0
    