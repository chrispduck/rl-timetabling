from typing import Iterable
from program_state import ProgramState

class Environment:
    """ Keeps track of the currents state
        Can be Queried for the next possible states
        Also calculates the reward for actions
    """
    
    def __init__(self, state):
        super().__init__()
        self.state = state 
    
    def query_possible_actions(self, state):
        #TODO
        """ Find all possible states S_{t+1}, reachable from S_t

        Args:
            state (ProgramState): current state S_t

        Returns:
            Iterable : An iterable containing each possible S_{t+1}
        """
        permissible_states = []
        return permissible_states

    def take_action(self, state, new_state):
        #TODO
        """ Updates the state stored in the environement
            Calculates the reward from such action

        Args:
            state (ProgramState): the old state
            new_state (ProgramState): the new state

        Returns:
            (ProgramState, float): 
        """
        reward = 0.
        return reward
    
    def reset():
        #TODO
        """ Resets the state of the 

        Returns:
            [type]: [description]
        """
        return 0
    