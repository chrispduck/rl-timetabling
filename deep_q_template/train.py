from agent import Agent
from environment import Environment
from neural_net import QNet
from program_state import ProgramState
import argparse
import json 

# Init parameters
parser = argparse.ArgumentParser()
parser.add_argument('--learning_rate', type=float, default=0.5, help='How quickly the algorithm tries to learn')
parser.add_argument('--discount', type=float, default=0.98, help='Discount for estimated future action')
parser.add_argument('--n_days', type=int, default=2000, help='Iteration count')
parser.add_argument('--n_employees', type=int, default=30, help='Total number of employees')
parser.add_argument('--n_attempts', type=int, default=10e5, help='number of attempts to schedule')
args, unparsed = parser.parse_known_args()

# Create NN to estimate Q values of actions
Q_model = QNet(n=args.n_employees)
# Create agent
agent = Agent(NN=Q_model)

# Create environment
state = ProgramState()
env = Environment(state=state)


for i in range(args.n_attempts):
    total_reward = 0

    for j in range(args.n_days):
        # Do learning
        old_state = environment.state # Retrieve current state
        new_state = agent.get_next_action(old_state) # Select the next action
        # Note we are not returning an action from get_next action. 
        # Instead we use new_state, which implicitly defines the action
        # This is OK because our actions have a deterministic effect on the environment. 
        # To generalise, an action must be included, and this action given tot he environment
        reward = environment.take_action(new_state)
        agent.update(old_state, new_state, reward) # Let the agent update Q network  
        
        total_reward += reward
