import random
import torch
from program_state import ProgramState
import numpy as np

class Agent:
    """ Acts as the agent in a deep Q learning problem, where Q values are
    estimated using a neural network
    """
    def __init__(self, NN, environment):
        """
        Args:
            NN (torch.nn.module): the neural network to estimate Q, inputs state, output Q
        """
        super().__init__()
        self.NN = NN
        self.environment = environment #needed for querying possible moves

    
    # Ask model to estimate Q value for specific state (inference)
    def get_Q(self, state):
        return self.NN.forward(state)
    
    def get_next_action(self, state):
        if random.random() > self.exploration_rate:  # Explore (gamble) or exploit (greedy)
            return self.greedy_action(state)
        else:
            return self.random_action()
    
    def greedy_action(self, state):
        """  
        # Finds and returns the new_state with the highest Q value, reachable via a permissible action
        # To do this it must query the environment for all next possible actions
        # All actions are evaluated by the Q

        Args:
            state (ProgramState): current state

        Returns:
            new_state (ProgramState) : the t+1 state with the highest Q value, reachable by a permissible action
        """
        permissible_new_states = self.envrionment.query_possible_actions(state)
        Q_values = np.zeros((len(permissible_new_states))

        for i, new_state in enumerate(permissible_new_states):    
            Q_values[i] = self.NN(new_state)
        
        # Index of the best move
        i = np.argmax(Q_values)
        new_state = permissible_new_states[i]
        return new_state
    
    def random_action(self):
        # Select an action at random from the acceptable actions
        # This is done by querying the environment 
        permissible_new_states = self.envrionment.query_possible_actions(state)
        state
        return 0
    
    def update(self, old_state, new_state, reward):
        # Update the Q network for the chosen action
        
        # Ask the model for the Q values of the old state (inference)
        Q_estimate = self.get_Q(old_state)  # Q_t

        # Ask the model for the Q values of the new state (inference)
        Q_tp1 = self.get_Q(new_state) # Q_{t+1}

        # Target Q value for the action we took. This is what we will train towards.
        Q_target = reward + self.discount * np.amax(Q_tp1)

        self.NN.train(estimate=Q_estimate, target=Q_target)
        
        return 0
    
